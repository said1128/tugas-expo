
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { AntDesign } from '@expo/vector-icons'; 
import Icon from 'react-native-vector-icons/MaterialIcons';
import { LinearGradient } from 'expo-linear-gradient';


export default class App extends Component {
  
  render() {
    return ( 
        <View style={styles.container}>
            <View style={styles.navBar}>
                <LinearGradient colors={['rgba(11, 6, 252, 0.77)', 'rgba(20, 255, 114, 0.7315)']} style={[styles.navBar,{ paddingHorizontal: 15,width:"100%",height:"100%"}]}>
                <View style={styles.leftNav}>
                    <TouchableOpacity>
                        <Icon style={styles.navItem} name="arrow-back" size={45} />
                    </TouchableOpacity>
                    <Image source={require('./images/logo.png')} style={{ width: 40, height: 40, marginTop:8}} />
                </View>
                <Text style={{ textAlign: "center",fontWeight: "bold",fontSize: 24}}>ABOUT</Text>
                <View style={styles.rightNav}>
                    <TouchableOpacity>
                    <Icon style={styles.navItem} name="account-circle" size={55} />
                    </TouchableOpacity>
                </View>
                </LinearGradient>
            </View>
            <View style={styles.body}>
                <View style={styles.card}>
                    <LinearGradient colors={['rgba(49, 60, 225, 0.79)', 'rgba(71, 231, 137, 0.38)']} style={{ paddingHorizontal: 15,width:"100%",height:"100%", borderRadius: 30}}>
                        <View style={styles.logo}>
                            <Image source={require('./images/profile.jpg')} style={styles.image} />
                            <Text style={styles.title}>Said Achmad</Text>
                        </View>
                        <View style={{ flex:1,flexDirection: 'column',justifyContent: 'space-between', margin:20}}>
                            <View style={styles.item}>
                            <AntDesign name="facebook-square" size={50} color="blue" />
                                <Text style={styles.itemTitle}>Said Achmad</Text>
                            </View>
                            <View style={styles.item}>
                            <AntDesign name="twitter" size={50} color="cyan" />
                                <Text style={styles.itemTitle}>Said1128_</Text>
                            </View>
                            <View style={styles.item}>
                            <AntDesign name="instagram" size={50} color="red" />
                                <Text style={styles.itemTitle}>Said1128_</Text>
                            </View>
                            <View style={styles.item}>
                            <AntDesign name="github" size={50} color="black" />
                                <Text style={styles.itemTitle}>Said1128</Text>
                            </View>
                        </View>
                    </LinearGradient>
                </View>
            </View>
        </View>        
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:20
      },
      navBar: {
        height: 55,
        backgroundColor: 'white',
        elevation: 3,
        flexDirection: 'row',
       
        alignItems: 'center',
        justifyContent: 'space-between'
      },
      rightNav: {
        flexDirection: 'row'
      },
      leftNav: {
        flexDirection: 'row'
      },
      navItem: {
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center'
      },
      body: {
        flex: 1,
      },
      card:{
        margin:30,
      },
    logo: {   
        flexDirection: 'column',
        marginTop:20,      
    },
    title:{
        textAlign: "center",
        fontWeight: "bold",
        fontSize: 36,    
    },
    image: {
    width: 200, height: 200, 
    borderRadius: 200/ 2,
    marginLeft:65,
    marginTop:10
    },
    item: {   
    flexDirection: 'row',
    marginTop:20, 
    alignItems: 'center',
    justifyContent: 'flex-start'     
    },
    itemTitle:{
        textAlign: "center",
        fontWeight: "bold",
        fontSize: 30,    
    },
});