
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';


export default class App extends Component {
  
  render() {
    return ( 
        
        <LinearGradient colors={['rgba(11, 6, 252, 0.77)', 'rgba(20, 255, 114, 0.7315)']} style={styles.container}>
            <View style={styles.logo}>
                <Image source={require('../Tugas13/images/logo.png')} style={styles.image} />
                <Text style={styles.title}>INFOSYS</Text>
            </View>
            <View style={styles.body}>
                <View>
                    <Text style={styles.contentTitle}>USERNAME</Text>
                    <TextInput style={styles.textInput} placeholder="Username"/>
                </View>
                <View>
                    <Text style={styles.contentTitle}>PASSWORD</Text>
                    <TextInput style={styles.textInput } secureTextEntry={true} placeholder="Password"/>
                </View>
                <TouchableOpacity style={{textAlign:"right"}}>
                    <Text style={[styles.contentTitle,{ textAlign:"right"}]}>FORGOT PASSWORD?</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{backgroundColor: "#DBFF00", borderRadius: 50,height:50}}
                 onPress={() =>
                    this.props.navigation.navigate("Drawer")
                  }>
                    <Text style={{ alignContent:"center", textAlign:"center",color: "#FFFFFF",fontSize: 22, lineHeight: 26, marginTop:10}}>LOGIN</Text>
                </TouchableOpacity>
               
            </View>
        </LinearGradient>

    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 35,
        paddingRight: 35,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {   
        flexDirection: 'column',
        marginTop:120,      
    },
    image: {
        width: 132,
        height: 114,
        marginLeft:50,
  },
    title:{
        textAlign: "center",
        fontWeight: "bold",
        fontSize: 48,       
        color: "#FFFFFF"
  },
    body: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        width:"100%",
        margin:50
    },
    textInput: {
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',  
        borderColor: 'white', 
        borderWidth: 1,
       
    },
    contentTitle:{
        fontFamily: "Roboto",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: 14,
        lineHeight: 21,
        alignItems: "center",
        color: "#DBFF00"
    }
});