
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { LinearGradient } from 'expo-linear-gradient';
import data from './skillData.json';
import SkillItem from './components/skillItem';

export default class App extends Component {
  
  render() {
    return ( 
        <View style={styles.container}>
            <View style={styles.navBar}>
                <LinearGradient colors={['rgba(11, 6, 252, 0.77)', 'rgba(20, 255, 114, 0.7315)']} style={[styles.navBar,{ paddingHorizontal: 15,width:"100%",height:"100%"}]}>
                <View style={styles.leftNav}>
                    <Image source={require('../Tugas13/images/logo.png')} style={{ width: 40, height: 40, marginTop:8}} />
                </View>
                <Text style={{ textAlign: "center",fontWeight: "bold",fontSize: 24}}>HOME</Text>
                <View style={styles.rightNav}>
                    <TouchableOpacity>
                    <Icon style={styles.navItem} name="account-circle" size={55} />
                    </TouchableOpacity>
                </View>
                </LinearGradient>
            </View>
            <View style={styles.body}>
                <FlatList
                    data={data.items}
                    renderItem={(skill)=><SkillItem data={skill.item} />}
                    keyExtractor={(item)=>item.id}
                    ItemSeparatorComponent={()=><View style={{height:10,backgroundColor:'#E5E5E5'}}/>}
                />                
            </View>
        </View>        
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:20
      },
      navBar: {
        height: 55,
        backgroundColor: 'white',
        elevation: 3,
        flexDirection: 'row',
       
        alignItems: 'center',
        justifyContent: 'space-between'
      },
      rightNav: {
        flexDirection: 'row'
      },
      leftNav: {
        flexDirection: 'row'
      },
      navItem: {
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center'
      },
      body: {
        
        flex: 1,
        backgroundColor:"#EBEBE9",
        justifyContent: 'space-between'
      },
    logo: {   
        flexDirection: 'column',
        marginTop:20,      
    },
    title:{
        textAlign: "center",
        fontWeight: "bold",
        fontSize: 36,    
    },
   
});