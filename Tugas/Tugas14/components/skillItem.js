import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class VideoItem extends Component {
    render() {
        let data = this.props.data;
        return (
            <View style={styles.container}>
                <Icon style={{color:'#003366', width: "20%",height: 80}} name={data.iconName} size={70} />
                <View style={styles.descContainer}>                
                    <Text style={{color:"#003366",fontWeight: "bold",fontSize: 24}}>{data.skillName}</Text>
                    <Text style={{color:"#3EC6FF",fontWeight: "bold",fontSize: 16}}>{data.categoryName}</Text>
                    <Text style={{color:"#FFFFFF",fontWeight: "bold",fontSize: 48,textAlign:"right"}}>{data.percentageProgress}</Text>
                </View>
                <Icon style={{color:'#003366', width: "20%", textAlign:"right"}} name="chevron-right" size={70} />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        padding: 15,
        margin: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius:20,
        backgroundColor:"#4167F3",
    },
    descContainer: {
        flexDirection: 'column',
        paddingTop: 15,
        width: "60%"
    }

});