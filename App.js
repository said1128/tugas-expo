import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import YouTubeUI from './Tugas/Tugas12/App';
import Tugas13Login from './Tugas/Tugas13/LoginScreen';
import Tugas13About from './Tugas/Tugas13/AboutScreen';
import Tugas14ToDoApp from './Tugas/Tugas14/App';
import Tugas14SkillScreen from './Tugas/Tugas14/SkillScreen';
import Tugas15TutorialNavigation from './Tugas/Tugas15/index';
import Tugas15ImplementNavigation from './Tugas/TugasNavigation/index';
import Quiz3 from './Quiz3/index';
export default function App() {
  return (
    <View style={styles.container}>
      {/* =======================Quiz 3========================= */}
      <Quiz3/>
      {/* =======================Quiz 3========================= */}

      {/* =======================TUGAS 15========================= */}
      {/* <Tugas15TutorialNavigation/> */}
      {/* <Tugas15ImplementNavigation/> */}
      {/* =======================TUGAS 15========================= */}

      {/* =======================TUGAS 14========================= */}
      {/* <Tugas14ToDoApp/> */}
      {/* <Tugas14SkillScreen/> */}
      {/* =======================TUGAS 14========================= */}

      {/* =======================TUGAS 13========================= */}
      {/* <Tugas13Login/> */}
      {/* <Tugas13About/> */}
      {/* =======================TUGAS 13========================= */}

      {/* =======================TUGAS 12========================= */}
      {/* <YouTubeUI/> */}
      {/* =======================TUGAS 12========================= */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});

